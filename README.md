# Industrialisation du Logiciel - TP1 - Tests, regression et performance

Ce TP a pour but de vous apprendre à utiliser les outils de test disponibles pour un projet python. Vous serez amené à manipuler les concepts suivants:
- Création de tests basiques (unitaires, intégration, fonctionnels)
- Création de tests de régression
- Création de tests de performance
- Utilisation du coverage

Les fichiers `wallet.py`, `matrix_calculation.py` et `string_manipulation.py` vous sont fournies dans le dossier `src`.


## 1. Tests basiques
- Créer des tests unitaires
- Créer des tests d'integration
- Créer des tests fonctionnels (Nécéssaires ? Possibles avec Sélénium mais il faudrait un usecase web)

Idée: modifier la shape d'une matrice après coup ('return [result]' par exemple)

## 2. Tests de régression
- Créer des tests de regressions

## 3. Tests fonctionnels
- Utiliser Selenium pour manipuler un navigateur web automatiquement

## 4. Tests de performance

Utiliser les librairies CProfile, pstas et snakeviz pour tester la performance du code et détecter les parties qui prennent le plus de temps
Expliquer l'output de la fonction run de CProfile. Comparer 2 implémentation d'une même fonction, comprendre que l'une prend plus de temps que l'autre grâce aux tests de performance

Utiliser Locust ? Bien pour les stress test, mais encore une fois il faudrait un usecase web

Tester avec Locust sur le site de la HE-Arc

## 5. Coverage
Le coverage est un très bon moyen de savoir à quel point votre code est testé.

idée: Demander aux élèves d'utiliser et d'expliquer la fonction coverage (pytest --cov=.). Se rendre compte que tous les modules pythons ne sont pas à 100%. Modifier les tests pour pouvoir atteindre les 100% de coverage
