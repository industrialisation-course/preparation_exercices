"""
This module gathers method to manipulate strings
"""


def capital_case(input_string: str) -> str:
    """
    Method that returns the upper case version of an input string
    """
    if not isinstance(input_string, str):
        raise TypeError("Please provide a string argument")
    return input_string.upper()


def lower_case(input_string: str) -> str:
    """
    Method that returns the lower case version of an input string
    """
    if not isinstance(input_string, str):
        raise TypeError("Please provide a string argument")
    return input_string.lower()


def invert(input_string: str) -> str:
    """
    Method that invert a string
    """
    if not isinstance(input_string, str):
        raise TypeError("Please provide a string argument")
    return input_string[::-1]
