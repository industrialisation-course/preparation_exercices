"""
Module that gathers methods to manipulate matrices
"""
import numpy as np


def mat_mult(X, Y):
    """Function that multiplies 2 matricies"""
    return np.matmul(X, Y)


def identity(order):
    return np.identity(order)
