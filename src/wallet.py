"""Package that provides a class Wallet"""


class InsufficientAmount(Exception):
    """Exception triggered when you try to spend more money than a wallet has"""

    pass


class Wallet(object):
    """Wallet class that lets your create a Wallet and add or spend money"""

    def __init__(self, initial_amount=0):
        self.balance = initial_amount

    def spend_cash(self, amount):
        """Removed the specified amount of money from the current Wallet"""
        if self.balance < amount:
            raise InsufficientAmount(f"Not enough available to spend {amount}")
        self.balance -= amount

    def add_cash(self, amount):
        """Adds the specified amount of money to the current Wallet"""
        self.balance += amount
