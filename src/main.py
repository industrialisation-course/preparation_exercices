from flask import Flask, request, abort

from src.features.math import add, sub, div, mult

app = Flask(__name__)


# Main route
@app.route("/")
def hello_world():
    return "The app is running correctly"


# Add route
@app.route("/add")
def add_route():
    a_number = request.args.get("a", type=int)
    b_number = request.args.get("b", type=int)

    if not a_number or not b_number:
        abort(
            400,
            "This route only supports two parameters a and b and they both must be integers.",
        )

    return {"a": a_number, "b": b_number, "result": add(a_number, b_number)}


# Sub route
@app.route("/sub")
def sub_route():
    a_number = request.args.get("a", type=int)
    b_number = request.args.get("b", type=int)

    if not a_number or not b_number:
        abort(
            400,
            "This route only supports two parameters a and b and they both must be integers.",
        )

    return {"a": a_number, "b": b_number, "result": sub(a_number, b_number)}


# mult route
@app.route("/mult")
def mult_route():
    a_number = request.args.get("a", type=int)
    b_number = request.args.get("b", type=int)

    if not a_number or not b_number:
        abort(
            400,
            "This route only supports two parameters a and b and they both must be integers.",
        )

    return {"a": a_number, "b": b_number, "result": mult(a_number, b_number)}


# mult route
@app.route("/div")
def div_route():
    a_number = request.args.get("a", type=int)
    b_number = request.args.get("b", type=int)

    if not a_number or not b_number:
        abort(
            400,
            "This route only supports two parameters a and b and they both must be integers.",
        )

    return {"a": a_number, "b": b_number, "result": div(a_number, b_number)}
