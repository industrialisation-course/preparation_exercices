def add(a, b):
    """Return the addition of a and b"""
    return a + b


def sub(a, b):
    """Return the substraction of a and b"""
    return a - b


def mult(a, b):
    """Return the multiplication of a and b"""
    return a * b


def div(a, b):
    """Return the division of a and b"""
    return a / b
