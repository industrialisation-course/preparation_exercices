import cProfile, pstats


def bad_array_filling():
    """Creates an array and fill it with values from 0 to 10'000 using an unoptimized code"""
    arr = []
    for i in range(100000):
        arr.append(i)
    print("Array created successfully")


def good_array_filling():
    """Creates an array and fill it with values from 0 to 10'000 using an optimized code"""
    arr = list(range(100000))
    print("Array created successfully")


if __name__ == "__main__":
    # Method 1 : Use run method to get basic ouput
    # cProfile.run("main()")

    # Method 2 : Use Profile class and pstats to sort the output the way you want
    profiler = cProfile.Profile()
    profiler.enable()
    bad_array_filling()
    good_array_filling()
    profiler.disable()
    stats = pstats.Stats(profiler).sort_stats("ncalls")
    stats.print_stats()
