"""
This class is meant to test Numpy's functionalities using a test class
"""
from tests.fixtures.numpy_fixutres import identity_matrix_3x3
from src.matrix_calulation import mat_mult, identity


def setup_module(module):
    """Function that is called before executing very tests of this module"""
    print("\n--> Setup module")


def teardown_module(module):
    """Function that is called after executing very tests of this module"""
    print("--> TearDown module")


class TestMatrixCalculation:
    """Test class meant to test Numpy's functionalities"""

    @classmethod
    def setup_class(cls):
        """Function that is called once before tests of this class are executed"""
        print("--> Setup class")

    @classmethod
    def teardown_class(cls):
        """Function that is called once after tests of this class are executed"""
        print("--> Teardown class")

    def setup_method(self, method):
        """Function called before each unit test of this class"""
        print("--> Setup method")

    def teardown_method(self, method):
        """Function called after each unit test of this class"""
        print("\n--> Teardown method")

    def test_matmult(self, identity_matrix_3x3):
        """Tests the matmul functionality of Numpy"""
        assert (
            mat_mult(identity_matrix_3x3, identity_matrix_3x3) == identity_matrix_3x3
        ).all()

    def test_identity(self, identity_matrix_3x3):
        """Tests identity function"""
        assert (identity(3) == identity_matrix_3x3).all()
