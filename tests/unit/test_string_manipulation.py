import pytest

from src.string_manipulation import lower_case, capital_case, invert


@pytest.mark.parametrize(
    "input_string, expected_output",
    [
        ("league of legends", "LEAGUE OF LEGENDS"),
        ("POKEMON", "POKEMON"),
        ("Street Fighter", "STREET FIGHTER"),
    ],
)
def test_capital_case(input_string, expected_output):
    """Tests the capital_case function"""
    assert capital_case(input_string) == expected_output


def test_lower_case():
    """Tests the lower_case function"""
    input_string = "AU REVOIR"
    expected_result = "au revoir"
    assert lower_case(input_string) == expected_result


def test_invert():
    """Tests the invert function"""
    input_stirng = "HE-Arc ingénierie"
    expected_string = "eireinégni crA-EH"
    assert invert(input_stirng) == expected_string


def test_raises_exception_on_non_string_arguments_capital_case():
    """Tests that capital_case function raises a TypeError when its input is not a string"""
    with pytest.raises(TypeError):
        capital_case(10)


def test_raises_exception_on_non_string_arguments_lower_case():
    """Tests that capital_case function raises a TypeError when its input is not a string"""
    with pytest.raises(TypeError):
        lower_case(10)


# Coverage for this class is 92% because we didn't test TypeError raise for invert method

# def test_raises_exception_on_non_string_arguments_invert():
#    """Tests that capital_case function raises a TypeError when its input is not a string"""
#    with pytest.raises(TypeError):
#        invert(10)
