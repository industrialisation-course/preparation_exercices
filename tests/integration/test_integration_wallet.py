"""Integration tests for Wallet package"""
import pytest
from tests.fixtures.wallet_fixtures import empty_wallet, wallet
from src.wallet import Wallet, InsufficientAmount


@pytest.mark.parametrize(
    "earned,spent,expected",
    [
        (30, 10, 20),
        (20, 2, 18),
    ],
)
def test_transactions(empty_wallet, earned, spent, expected):
    empty_wallet.add_cash(earned)
    empty_wallet.spend_cash(spent)
    assert empty_wallet.balance == expected


# Partir d'un usecase dans la donnée pour créer un test d'intégration (proposer un scénario). (example: paiement différé)
