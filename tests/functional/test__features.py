"""Module that tests the app"""
import pytest
from src.features.math import add, mult, div, sub


def test_add():
    """Test the add method"""
    assert add(2, 3) == 5


def test_sub():
    "Test the sub method"
    assert sub(0, 4) == -4


def test_mult():
    """Test the mult method"""
    assert mult(5, 10) == 50


def test_div():
    """Test the div method"""
    assert div(3, 2) == 3 / 2


def test_div_error():
    """Test that div methods returns an error whent trying to divide by 0"""
    with pytest.raises(ZeroDivisionError):
        div(10, 0)
