def test_index_route(client):
    """
    Testing the main route
    """
    response = client.get("/")
    assert response.status_code == 200
    assert b"The app is running correctly" in response.data


def test_add_route(client):
    """
    Testing the add route
    """
    response = client.get("/add", query_string={"a": 2, "b": 3})
    assert response.status_code == 200
    assert b"5" in response.data


def test_sub_route(client):
    """
    Testing the sub route
    """
    response = client.get("/sub", query_string={"a": 2, "b": 3})
    assert response.status_code == 200
    assert b"-1" in response.data


def test_mult_route(client):
    """
    Testing the mult route
    """
    response = client.get("/mult", query_string={"a": 2, "b": 3})
    assert response.status_code == 200
    assert b"6" in response.data


def test_div_route(client):
    """
    Testing the div route
    """
    response = client.get("/div", query_string={"a": 10, "b": 2})
    assert response.status_code == 200
    assert b"5" in response.data
