from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import time


class TestSeleniumFunctional:
    """This is a example class on how to use Selenium"""

    def test_open_google_and_search_hearc(self):
        """This is a test example that open Firefox, go on Google and search 'He-Arc'"""
        self.browser = webdriver.Firefox(executable_path="./geckodriver.exe")
        self.browser.get("https://www.google.com")
        accept_cookies = self.browser.find_elements(By.XPATH, "//button")[3]
        accept_cookies.click()

        search_bar = self.browser.find_element(
            By.XPATH, "//form[@action='/search']"
        ).find_element(By.XPATH, "//input")
        search_bar.send_keys("He-Arc")

        search_bar.send_keys(Keys.ENTER)

        time.sleep(10)
        self.browser.close()
